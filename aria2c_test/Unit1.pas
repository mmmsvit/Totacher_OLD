unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,

  aria2c, Vcl.ComCtrls, Vcl.ToolWin, Vcl.ImgList,

  System.ImageList, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ListView1: TListView;
    ToolButton9: TToolButton;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
    procedure EnableButtons(serverwork: boolean);
  public
    { Public declarations }
    procedure AddLog(s: string);
  end;

var
  Form1: TForm1;
  aria: TAria2;
  sApp: string;

implementation

{$R *.dfm}

procedure TForm1.EnableButtons(serverwork: boolean);
begin
  ToolButton2.Enabled:=serverwork;
  ToolButton1.Enabled:= not serverwork;
end;

procedure TForm1.AddLog(s: string);
begin
  Memo1.Lines.Append(s);
end;


procedure TForm1.Button1Click(Sender: TObject);
var
  b: Boolean;
begin
  // AddLog('start: '+BoolToStr(aria.start(True), False));
  b:=aria.start(True);
  EnableButtons(b);
  AddLog('start: '+BoolToStr(b, True));
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  b: Boolean;
begin
  b:=aria.stop;
  AddLog('stop: '+BoolToStr(b, True));
  EnableButtons(not b);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  EnableButtons(False);
  sApp:=IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)))+'aria2c.exe';

//  aria:=TAria2.Create(sApp, ' --log=aria2c.log --user-agent=googlebot');
//  aria:=TAria2.Create(sApp, ' --log=- --user-agent=googlebot');
  aria:=TAria2.Create(sApp, '--max-connection-per-server=10 --split=10');
  aria.OnAppengLogMessage:=AddLog;  // asign log memo
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   aria.Free;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  ToolButton1.Click;
  ToolButton5.Click;
end;

procedure TForm1.ToolButton5Click(Sender: TObject);
begin
  aria.addUri('http://www.totacher.zz.mu/STALKER_OP2.7z');
//    + ';https://dl.dropboxusercontent.com/content_link/cBkDwx6ggdRLkuK0LLEUJkRloihbLk8KDkubDNianQHv0SlFiQp2Kqt2FAc8LekL?dl=1');
end;

end.
